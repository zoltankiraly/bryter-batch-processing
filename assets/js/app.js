const SYMBOL_FOR_RANGE = "-";
const SYMBOL_FOR_LIST = ",";

window.addEventListener("DOMContentLoaded", () => {

    let form = document.getElementById("form");
    let status = document.getElementById("status");

    // Run the following upon form submit
    form.addEventListener("submit", function (e) {
        //Prevent form to submit
        e.preventDefault();

        //Change submit button to disabled, in order to prevent multiple submits.
        document.getElementById("submit").setAttribute("disabled", "disabled");

        //Show progress
        status.classList.add("active");

        //Show iframe
        document.getElementById("iframe").classList.add("active");

        let config = {
            timer : 5000,
            moduleUrl : 'https://tenant.bryter.io/xyz',
            parameterName : 'id'
        };

        //Create the ids variable
        let ids = retrieveIds(document.getElementById("ids_to_process").value);

        // Start processing the ids
        processAllIds(ids, config);

        //Calculate total remaining time and display it
        displayTotalTimeUntilFinished(ids, config)
    });
});

function processAllIds(ids, config) {
    if (ids.length > 0) {
        let index = 0;
        processId(index, ids, config);
        if (ids.length > 1) {

            let loadFrame = setInterval(function () {
                index++;

                //Create the iframe
                processId(index, ids, config);

                // When end of array reached, stop processing and clearInterval()
                if (ids.length - 1 === index) {
                    showFinishedScreen(ids, config);
                    clearInterval(loadFrame);
                }
            }, config.timer);

        } else {
            showFinishedScreen(ids, config);
        }
    }
}

function processId(index, ids, config) {
    // Update iframe src.
    document.getElementById("iframe").src = config.moduleUrl + "?" + config.parameterName + "=" + ids[index];

    displayCurrentModuleTime(index, ids, config);
}

function displayCurrentModuleTime (index, ids, config) {
    // Update  #current-id  with current index.
    document.getElementById("current-id").textContent = ids[index];

    // Update current ID remaining time.
    let timeUntilChange = Math.floor((config.timer / 1000) % 60);
    let moduleReload = setInterval(function () {
        document.getElementById("current-id-remaining-time").textContent = timeUntilChange;

        --timeUntilChange;

        if (timeUntilChange === -1) {
            clearInterval(moduleReload);
        }
    }, 1000);
}

function displayTotalTimeUntilFinished(ids, config) {
    let display = document.getElementById("current-batch-remaining-time");

    let duration = ids.length * (Math.floor((config.timer / 1000) % 60));

    let minutes, seconds;
    setInterval(function () {
        if (minutes < 0 || (minutes === 0 && seconds <= 0)) {
            return;
        }

        minutes = parseInt(duration / 60);
        seconds = parseInt(duration % 60);

        display.textContent = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);

        if (--duration < 0) {
            return;
        }
    }, 1000);
}

function retrieveIds(textarea) {
    // Create an array of ids from textarea values.
    if (textarea.includes(SYMBOL_FOR_RANGE)) {
        let ids = [];
        let split = textarea.split(SYMBOL_FOR_RANGE).map(Number);
        for (let i = split[0]; i <= split[1]; i++) {
            ids.push(parseInt(i));
        }
        return ids;

    } else {
        return textarea.split(SYMBOL_FOR_LIST).map(Number);
    }
}

function showFinishedScreen(ids, config) {
    setTimeout(function () {
        document.getElementById("iframe").classList.add("finished");
        document.getElementById("finished").classList.add("active");

        document.getElementById("finished-ids").textContent = ids.toString();
    }, config.timer);
}
