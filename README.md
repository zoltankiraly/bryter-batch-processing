# Introduction

This simple application is a demo of how you batch process your BRYTER modules. It is written in html and native javascript, so no libraries or dependencies are required. 

By entering the values that you would like the process into the textarea, it creates an array and loops through it.

# Requirements

The module must start with an action node, and that action node must be an URL parameter.

# Configuration

1. Open `assetes/js/app.js` and configure the script to your requirements using the `config` object set the `timer, moduleUrl and parameterName` to your desired value.

